# Carte d'Éligibilité FTTH Axione/CityFast

Ce dépot contient deux outils pour consulter l'API publique d'éligibilité fibre d'Axione.
Un script de pré-téléchargement des addresses éligibles, et une page web d'affichage des points récupérés.

Ces outils sont à destination de bricoleurs en informatique ayant un minimum de savoir-faire.
(Éditer un fichier texte, lancer un script Python, etc.)
Ils ont été conçus sur et pour Linux, tout autre utilisation sera de votre propre artisanat.

## Configuration

Par défaut, ce dépot est configuré pour télécharger et afficher la métropole Grenobloise.
Si vous souhaitez changer ces paramètres, éditez [`download_elig_data.py`](download_elig_data.py) et [`index.html`](index.html)

## [Installer Nix][install_nix]

Avec [nix installé][install_nix] vous pouvez lancer les commandes suivantes directement.
Sinon, vous devrez vous débrouiller pour installer les dépendences.

[install_nix]: https://nixos.org/download.html

## Télécharger les données d'Axione

```sh
./download_elig_data.py
```

🛈 Avec la configuration par défaut le téléchargement prend ~14min.

## Afficher les données sur une carte

```
./serve.py
```

Ouvrez dans un navigateur internet le lien qui s'affiche.

## À l'attention d'Axione

Si vous travaillez chez Axione, veuillez considérer le code de ce dépot, ainsi que le présent document comme une lettre ouverte, vous invitant poliment à améliorer votre interface.

En effet, ces outils ont été développés dans l'unique but d'aider les adhérents de l'association [Rézine](rezine.org) à savoir s'ils sont éligibles à la fibre via votre service, et parce que votre [page web publique](https://e-ftth.axione.fr/eligftth3/gui/public/carte.htm) est dans la pratique inutilisable.
(Je ne l'ai vue qu'une-seule fois afficher des résultats, et ce n'était pas pour Grenoble.)

Si vous souhaitez me contacter, vous pouvez le faire à l'addresse suivante: thibault.lemaire@protonmail.com.

L'opinion exprimée ici est la mienne, et ne représente en aucun cas une posture politique de l'association Rézine.
